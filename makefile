RUST_DIST = nightly
RUSTC = multirust run $(RUST_DIST)

build:
	$(RUSTC) cargo build

release:
	$(RUSTC) cargo build --release

run:
	$(RUSTC) cargo run

install: release
	@mv ./target/release/delta /usr/local/bin

install-debug: build
	@mv ./target/debug/delta /usr/local/bin