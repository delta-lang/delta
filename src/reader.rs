use std::path::Path;
use std::fs::File;
use std::io::Read;

use delta_lang::ScriptUnit;
use error::*;

pub struct ScriptReader {
	unit: ScriptUnit
}

impl ScriptReader {
	pub fn new_from_filename(filename: String) -> Result<Self, CLIError> {
		let path = Path::new(&*filename);
		if path.exists() {
			return match File::open(path) {
				Ok(ref mut file) => Self::new_from_file(file),
				Err(e) => Err(CLIError::new(CLIErrorKind::IoError(e)))
			}
		}
	    return Err(NoFileError::new(filename.clone(), Default::default()).into()); 
	}

	fn new_from_file(file: &mut File) -> Result<Self, CLIError> {
		let mut s = String::new();
		if let Err(e) = file.read_to_string(&mut s) {
			return Err(CLIError::new(CLIErrorKind::IoError(e)));
		}
		let unit = try!(ScriptUnit::new(&*s).map_err(|e| CLIError::from(e)));
		Ok(ScriptReader::new(unit))
	}

	pub fn new(unit: ScriptUnit) -> Self {
		ScriptReader {
			unit: unit
		}
	}
}

impl Into<ScriptUnit> for ScriptReader {
	fn into(self) -> ScriptUnit {
		self.unit
	}
}