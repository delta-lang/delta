use std::io;
use std::io::prelude::*;

use context::Context;
use delta_lang::ScriptUnit;

pub struct Repl {
	ctx: Context
}

impl Repl {
	pub fn new() -> Self {
		Repl {
			ctx: Context::new()
		}
	}

	pub fn run(&mut self) {
		let stdin = io::stdin();
		for line in stdin.lock().lines() {
			let input = line.unwrap();
			match ScriptUnit::new(&*input) {
				Ok(unit) => self.ctx.run(unit),
				Err(err) => println!("{:?}", err)
			};
		}
	}
}

