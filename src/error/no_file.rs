use std::default::Default;
use error::*;

#[derive(Debug, Clone)]
pub struct NoFileError {
	filename: String,
	message: String
}

impl NoFileError {
	pub fn new(filename: String, options: NoFileOptions) -> Self {
		NoFileError {
			filename: filename,
			message: options.message
		}
	}
}

impl Into<CLIError> for NoFileError {
	fn into(self) -> CLIError {
		CLIError::new(CLIErrorKind::NoFile(self.clone()))
	}
}

pub struct NoFileOptions {
    message: String
}

impl Default for NoFileOptions {
	fn default() -> NoFileOptions {
		NoFileOptions {
			message: "No such file.".into()
		}
	}
}