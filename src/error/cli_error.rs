use delta_lang::ScriptError;
use error::*;
use std::io::Error as IoError;

#[derive(Debug)]
pub struct CLIError {
	kind: CLIErrorKind
}

impl CLIError {
    pub fn new(kind: CLIErrorKind) -> Self {
    	CLIError {
    		kind: kind
    	}
    }
}

impl From<ScriptError> for CLIError {
    fn from(err: ScriptError) -> Self {
        CLIError::new(CLIErrorKind::Script(err))
    }
}

#[derive(Debug)]
pub enum CLIErrorKind {
	Script(ScriptError),
	NoFile(NoFileError),
    IoError(IoError)
}