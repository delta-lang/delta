extern crate delta_lang;
extern crate delta_stdlib;
extern crate rustc_serialize;
extern crate docopt;

mod error;
mod reader;
mod repl;
mod context;

use docopt::Docopt;
use self::reader::ScriptReader;
use self::context::Context;
use self::repl::Repl;

const DELTA_VERSION: &'static str = "0.1.0";

const USAGE: &'static str = "
	The Delta programming language.

	Usage:
	  delta <file>
	  delta (-i | --interactive)
	  delta (-h | --help)
	  delta (-v | --version)

	Options:
      -i --interactive     Interactive environment.
	  -h --help     Show this screen.
	  -v --version     Show version.
	";

#[derive(Debug, RustcDecodable)]
struct Args {
    arg_file: String,
    flag_version: bool,
    flag_interactive: bool
}

fn main() {
	let args: Args = Docopt::new(USAGE)
	                    .and_then(|d| d.decode())
	                    .unwrap_or_else(|e| e.exit());
    
    if args.flag_version {
    	return println!("Delta {}", DELTA_VERSION.to_owned());
    }

    if args.flag_interactive {
        let mut repl = Repl::new();
        return repl.run();
    }

    run_script_from_file(args.arg_file);
}

fn run_script_from_file(filename: String) {
    match ScriptReader::new_from_filename(filename) {
        Ok(reader) => {
            let mut ctx = Context::new();
            ctx.run(reader.into());
        },
        Err(err) => println!("{:?}", err)
    }
}