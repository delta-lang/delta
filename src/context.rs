use delta_lang::{ScriptUnit, Runtime};
use delta_stdlib::init;

pub struct Context {
	runtime: Runtime
}

impl Context {
	pub fn new() -> Self {
		let mut runtime = Runtime::new();
		runtime.load_package(init());
		Context {
			runtime: runtime
		}
	}

	pub fn run(&mut self, unit: ScriptUnit) {
		match self.runtime.eval_unit(unit) {
			Err(err) => println!("{:?}", err),
			Ok(_) => {}
		}
	}
}